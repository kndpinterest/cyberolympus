#### PHP LARAVEL WITH REACT.JS (REDUX,TSX)

How to use

- first you need install module from composer and cli
- this must use php version 7.3 require
- composer install if done
- you need install package from cli npm install after finish you mush open folder laravel-mix for location laralve-mix/src/builder/webpack-rules.js if you get this file you need add this script

```
rules.push({
  test: /\.(ts|tsx)$/,
  loader: "ts-loader",
  exclude: /node_modules/
  })
```

- and then open file WebpackConfig.js from folder laravel-mix/src/builder/WebpackConfig.js and then add

```
buildResolving() {
  this.webpackConfig.resolve = {
    extensions: [".tsx", ".ts"]
    }
  }
```

- add Database.sql location filename -> SOAL\ DAN\ PETUNJUK/Database.sql
- how to push to mysql -> mysql -u root -p new_database < Database.sql
- and then you need configure with file environment in the folder PROJECT/.env

- table

  ![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/164690134_1372301909769893_7129154093859400069_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeG1pOYK2e8eTrYZQjg7FsLJ1YdsV1niJMLVh2xXWeIkwjlN1yxBG_S2hUywQ6wVjK0lrIqlbYJeP1AsM8ereemj&_nc_ohc=y6aTgKNpyXsAX9WDvQf&_nc_ht=scontent.fsrg1-1.fna&oh=553135c76afe02d813e179b184914c89&oe=6080C547)

- register accounts

![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/163636480_1371021889897895_7008311331169889014_o.jpg?_nc_cat=103&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGUIQ17BQetDO7_VIx4RK5gryx-0tReKiOvLH7S1F4qI6R36LoTkad2IJe73ESIDKpEXztCWyevaZXYsqoWVE1t&_nc_ohc=E3u24Af376IAX8sY1ex&_nc_ht=scontent.fsrg1-1.fna&oh=9d142c14dff76999b0118736a46c522a&oe=6081BEF9)

- login accounts

![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/163010007_1371021893231228_4428644493969597415_o.jpg?_nc_cat=101&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeHOw74ahJEqCRLXo969qgf-vIwYh3vu9z28jBiHe-73Pbd9q0rqJyEv2Db3e4SG4NVIkoj9_CVq9QtDXuHn-BdR&_nc_ohc=kaE0QyUG_l0AX_tY8PH&_nc_ht=scontent.fsrg1-1.fna&oh=dcd67d9cac5b6a82683ddfa40c6c2f12&oe=60807254)

- web products -> This does not support mobile, because I do not provide css for the mobile version

![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/163847322_1372305193102898_4133759820980223929_o.jpg?_nc_cat=102&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeHDQMLUeHb4MltESBnS3m3HsgY0X4OrqUOyBjRfg6upQ1-b-82uexRvP8iHhTRgx8uQo1r1EIzw-Jwr2zrEgva_&_nc_ohc=Tt7qmdO736cAX-w-zhV&_nc_ht=scontent.fsrg1-1.fna&oh=f49482fe3cff2a69bc6b71ede709f71e&oe=607F1C26)
