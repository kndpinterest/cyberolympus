<?php

namespace App\Http\Resources;

use App\ProductCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResourceJson extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'category' => ProductCategory::find($this->category)->name,
            'type' => $this->type,
            'item' => $this->item,
            'weight' => $this->weight,
            'sku' => $this->sku,
            'price_sell' => 'Rp '. number_format($this->price_sell,0) .',-',
            'price_sell_input' => $this->price_sell,
            'price_promo' => $this->price_promo,
            'price_agent' => $this->price_agent,
            'photo' => $this->photo,
            'recommendation' => $this->recommendation,
            'description' => $this->description,
            'status' => $this->status,
            'ordering' => $this->ordering,
        ];
    }
}
