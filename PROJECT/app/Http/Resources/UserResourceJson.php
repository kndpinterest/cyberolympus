<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResourceJson extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "account_role"  => $this->account_role,
            "account_status" => $this->account_status,
            "account_type"  => $this->account_type,
            "device_token"  => $this->device_token,
            "email"  => $this->email,
            "email_verified_at"  => $this->email_verified_at,
            "first_name"  => $this->first_name,
            "id"  => $this->id,
            "last_login"  => $this->last_login,
            "last_name"  => $this->last_name,
            "password_reset_code"  => $this->password_reset_code,
            "phone"  => $this->phone,
            "phone_verified_at"  => $this->phone_verified_at,
            "photo"  => Storage::disk("local")->exists($this->photo) ? $this->photo : env("url") . '/images/' . '86aa20ef1dc92946c161f2e361bb8bc0-003011100_1478521200-aceshowbiz.jpeg',
            "pin" => $this->pin,
        ];
    }
}
