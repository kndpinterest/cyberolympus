<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous" />
    <style>
        @import url("https://fonts.googleapis.com/css2?family=Hind+Madurai:wght@500&display=swap");

        body {
            margin: auto;
            font-family: "Hind Madurai", sans-serif;
        }

        ::-webkit-scrollbar {
            width: 1.5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: gray;
            border: none;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: gray;
        }

        body {
            margin: auto;
            padding: auto;
        }

        .kkn-nav {
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            display: flex;
            align-items: center;
            justify-content: space-around;
            height: 80px;
            background-color: white;
        }

        .kkn-nav-right-list {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .kkn-nav-image-author {
            width: 50px;
            height: 50px;
            border-radius: 80px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
            margin-right: 5px;
        }

        .kkn-nav .kkn-nav-logo {
            width: 200px;
            height: 40px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
        }

        .kkn-btn-general {
            background-color: red;
            color: white;
            width: 120px;
            border: none !important;
            outline: none !important;
            height: 32px;
            text-decoration: none;
            margin-right: 5px;
            margin-left: 5px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            font-weight: bold;
            text-transform: capitalize;
        }

        .kkn-form {
            width: 100%;
            height: 80vh;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #kkn-field {
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
        }

        .kkn-field {
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: white;
            width: 460px;
            height: 40px;
            margin-bottom: 10px;
            border-radius: 15px;
            overflow: hidden;
            background-color: transparent;
        }

        .kkn-field i {
            width: 40px;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: red;
            font-size: 16px;
            color: white;
        }

        .kkn-field input {
            width: 100%;
            height: 100%;
            border: none;
            outline: none;
            padding-left: 15px;
            padding-right: 15px;
        }

        .kkn-btn-form {
            width: 160px;
            height: 32px;
            background-color: red;
            border-radius: 20px;
            border: none;
            outline: none;
            color: white;
            text-transform: capitalize;
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
        }

        button {
            border: none !important;
            outline: none !important;
            cursor: pointer !important;
        }

        a {
            cursor: pointer !important;
        }

        .kkn-message-success,
        .kkn-message-error,
        .kkn-message-close {
            width: 360px;
            padding-top: 6px;
            padding-bottom: 6px;
            border-radius: 10px;
            padding-left: 10px;
            padding-right: 10px;
            position: fixed;
            color: white;
            display: flex;
        }

        .kkn-message-success span,
        .kkn-message-error span,
        .kkn-message-close span {
            width: 100%;
            user-select: none;
        }

        .kkn-message-success {
            background-color: green;
        }

        .kkn-message-error {
            background-color: red;
        }

        #message-open {
            animation: message-open 2s;
            animation-fill-mode: forwards;
        }

        #message-close {
            animation: message-close 2s;
            animation-fill-mode: forwards;
        }

        @keyframes message-open {
            from {
                left: -1200px;
            }

            to {
                bottom: 10px;
                left: 10px;
            }
        }

        @keyframes message-close {
            from {
                bottom: 10px;
                left: 10px;
            }

            to {
                left: -1200px;
            }
        }

        .kkn-btn-message {
            border: none;
            outline: none;
            cursor: pointer;
            width: 30px;
            height: 30px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 10px;
        }

        #kkn-color-icon-success {
            color: green;
        }

        #kkn-color-icon-error {
            color: red;
        }

        .kkn-btn-message i {
            font-size: 20px;
        }

        #visibility {
            display: none;
            visibility: hidden;
        }

        .kkn-grid {
            display: flex;
            justify-content: space-around;
            margin-top: 15px;
        }

        .kkn-col {
            width: 48%;
            background-color: white;
            padding: 15px;
        }

        .kkn-products-list-name {
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 25px;
            margin-top: 5px;
            font-weight: bold;
            text-transform: capitalize;
        }

        .kkn-card-products {
            display: flex;
            height: 100px;
            padding: 4px;
            background-color: white;
            border-radius: 10px;
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            margin-bottom: 5px;
        }

        .kkn-card-products img {
            width: 100px;
            height: 100%;
            border-radius: 10px;
            margin-right: 5px;
        }

        .kkn-card-list-product {
            display: flex;
            flex-direction: column;
            width: 100%;
        }

        .kkn-card-list-product-names {
            line-height: 16px;
            display: flex;
            flex-direction: column;
        }

        .kkn-card-name-category-product {
            color: #bbb9b9;
        }

        .fas.fa-star {
            color: #bbb9b9;
        }

        .kkn-card-ranting-value-product {
            color: #bbb9b9;
        }

        .kkn-card-products-minus {
            cursor: pointer;
            font-size: 24px;
        }

        .kkn-card-products-plus {
            cursor: pointer;
            font-size: 24px;
        }

        .kkn-card-products-cart {
            display: flex;
            flex-direction: column;
            width: 120px;
            justify-content: center;
            margin-right: 10px;
        }

        .kkn-card-products-cart .kkn-card-products-cart-group {
            display: flex;
            align-items: center;
            justify-content: space-around;
            margin-bottom: 15px;
        }

        .kkn-card-products-btn {
            width: 120px;
            height: 32px;
            border-radius: 10px;
            background-color: red;
            box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px -7px rgba(0, 0, 0, 0.75);
            color: white;
        }

        .kkn-products-total {
            width: 80%;
            margin: auto;
            display: flex;
            justify-content: space-between;
            flex-direction: column;
            margin-top: 20px;
        }
        .kkn-products-total-list {
            display: flex;
            justify-content: space-between;
            font-weight: bold;
        }
    </style>

</head>

<body>
    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>
</body>

</html>