import { Reducer } from "redux";
import { ProductState, ProductTypes } from "../constant/productTypes";

const initialState: ProductState = {
    product: [],
    data: {},
    cart: {
        total_item: "0",
        sub_total: "0",
        quantity: "0",
        product: []
    },
    loading: false
};

export const productReducer: Reducer<ProductState> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case ProductTypes.LOAD_PRODUCT:
            return {
                ...state,
                data: action.payload.product
            };
            break;
        case ProductTypes.ADD_CART:
            return {
                ...state,
                cart: {
                    total_item: action.payload.total_item,
                    sub_total: action.payload.sub_total,
                    quantity: action.payload.quantity,
                    product: action.payload.product
                }
            };
            break;
        case ProductTypes.REMOVE_CART:
            return {
                ...state,
                cart: {
                    total_item: action.payload.total_item,
                    sub_total: action.payload.sub_total,
                    quantity: action.payload.quantity,
                    product: action.payload.product
                }
            };
            break;
        default:
            return state;
            break;
    }
};
