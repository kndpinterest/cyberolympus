import { Reducer } from "redux";
import { UserState, UserTypes } from "../constant/userTypes";

const initialState: UserState = {
    user: [],
    data: {},
    token: null
};

export const userReducer: Reducer<UserState> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case UserTypes.LOGIN:
            return {
                ...state,
                token: action.payload.token
            };
            break;
        case UserTypes.LOGOUT:
            return {
                ...state,
                token: null
            };
            break;
        case UserTypes.REGISTER:
            return {
                ...state
            };
        case UserTypes.LOAD_ME:
            return {
                ...state,
                data: action.payload.me
            };
            break;
        default:
            return state;
            break;
    }
};
