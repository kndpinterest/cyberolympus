import { Reducer } from "redux";
import { MessageState, MessageTypes } from "../constant/messageTypes";

const initialState: MessageState = {
    message: {
        message: "",
        status: "visibility",
        statusCode: 400
    }
};

export const messageReducer: Reducer<MessageState> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case MessageTypes.OPEN:
            return {
                ...state,
                message: {
                    message: action.payload.message,
                    status: action.payload.status,
                    statusCode: action.payload.statusCode
                }
            };
            break;
        case MessageTypes.CLOSE:
            return {
                ...state,
                message: {
                    message: state.message.message,
                    status: "close",
                    statusCode: state.message.statusCode
                }
            };
        case MessageTypes.VISIBILITY:
            return {
                ...state,
                message: {
                    message: state.message.message,
                    status: "visibility",
                    statusCode: state.message.statusCode
                }
            };
            break;
        default:
            return state;
            break;
    }
};
