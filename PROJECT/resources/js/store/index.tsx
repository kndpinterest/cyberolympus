import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { History } from "history";
import { userReducer } from "./reducer/userReducer";
import { messageReducer } from "./reducer/messageReducer";
import { productReducer } from "./reducer/productReducer";

const stores = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        user: userReducer,
        message: messageReducer,
        product: productReducer
    });

export default stores;
