import { Message } from "./types";

export enum MessageTypes {
    OPEN = "[MESSAGE] :: OPEN",
    CLOSE = "[MESSAGE] :: CLOSE",
    VISIBILITY = "[MESSAGE] :: VISIBILITY"
}

export interface MessageState {
    readonly message: Message;
}
