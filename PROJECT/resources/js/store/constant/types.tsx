export interface User {
    id?: string;
    name?: string;
    first_name?: string;
    last_name?: string;
    email_verified_at?: string;
    email?: string;
    password?: string;
    confirm_password?: string;
    pin?: string;
    confirm_pin?: string;
    phone?: string;
    phone_verified_at?: Date;
    account_type?: string;
    account_role?: string;
    photo?: string;
    last_login?: Date;
    account_status?: string;
    fcm_token?: string;
}

export type Status = "open" | "close" | "visibility";

export interface Message {
    message?: string;
    status: Status;
    statusCode: number;
}

export interface Product {
    id?: string;
    product_name?: string;
    category?: string;
    type?: string;
    item?: string;
    weight?: string;
    sku?: string;
    price_sell?: string;
    price_sell_input?: number;
    price_promo?: string;
    price_agent?: string;
    photo?: string;
    recommendation?: string;
    description?: string;
    status?: string;
    ordering?: string;
}

export interface ResultProducts {
    data?: Product[];
    count?: string;
    current_page?: string;
    first_item?: string;
    get_url_range?: string;
    last_item?: string;
    last_page?: string;
    on_first_page?: string;
    per_page?: string;
    next_page_url?: string;
    prev_page_url?: string;
}

export interface CartProduct {
    quantity: string;
    sub_total: string;
    total_item: string;
    product: Product[];
}
