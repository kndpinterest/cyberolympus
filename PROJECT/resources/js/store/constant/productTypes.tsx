import { CartProduct, Product, ResultProducts } from "./types";

export enum ProductTypes {
    LOAD_PRODUCT = "[PRODUCT] :: LOAD_PRODUCT",
    LOADING_PRODUCT = "[PRODUCT] :: LOADING_PRODUCT",
    ADD_CART = "[PRODUCT] :: ADD_CART",
    REMOVE_CART = "[PRODUCT] :: REMOVE_CART"
}

export interface ProductState {
    readonly product: Product[];
    readonly cart: CartProduct;
    readonly data: ResultProducts;
    readonly loading: boolean;
}
