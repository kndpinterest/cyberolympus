import { User } from "./types";

export enum UserTypes {
    LOGIN = "[USER] :: LOGIN",
    REGISTER = "[USER] :: REGISTER",
    LOGOUT = "[USER] :: LOGOUT",
    LOAD_ME = "[USER :: LOAD_ME"
}

export interface UserState {
    readonly user: User[];
    readonly data: User;
    readonly token: any;
}
