import axios, { AxiosResponse } from "axios";
import { Dispatch } from "redux";
import { controllerNetwork } from "../../utils/network";
import { ProductTypes } from "../constant/productTypes";
import { Product } from "../constant/types";

export const loadProduct = () => {
    return async (dispatch: Dispatch) => {
        const response = await axios
            .get(`${controllerNetwork}/api/products`, {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "POST",
                    "Access-Control-Allow-Origin": controllerNetwork,
                    "Access-Control-Allow-Headers":
                        "Content-Type, Origin, Accepted, X-Requested-With",
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                },
                timeout: 865000,
                responseType: "json",
                withCredentials: false,
                maxRedirects: 5,
                maxContentLength: 2000,
                validateStatus: (status: number) =>
                    status >= 200 && status < 300,
                transformRequest: function request(res) {
                    dispatch({
                        type: ProductTypes.LOADING_PRODUCT
                    });
                    return JSON.stringify(res);
                }
            })
            .then((res: AxiosResponse<any>) => {
                dispatch({
                    type: ProductTypes.LOAD_PRODUCT,
                    payload: {
                        product: res.data
                    }
                });
            })
            .catch(err => {
                console.log(err);
            });
        return response;
    };
};

export const addCart = (
    args: string,
    product: Product[],
    _total_item: string,
    _sub_total: string,
    _quantity: string,
    addToCart: Product[]
) => {
    return async (dispatch: Dispatch) => {
        let total_item, sub_total, quantity, addToCarts: Product[];

        const filters_ = product.filter(function(x) {
            return x.id === args;
        })[0];

        const check = addToCart.filter(function(x) {
            return x.id === args;
        })[0];

        if (check) {
            console.log("hell worlds");
            addToCarts = addToCart.map(x =>
                x.id === args
                    ? {
                          id: x.id,
                          product_name: x.product_name,
                          category: x.category,
                          type: x.type,
                          item: x.item,
                          weight: x.weight,
                          sku: x.sku,
                          price_sell: x.price_sell,
                          price_sell_input: x.price_sell_input,
                          price_promo: x.price_promo,
                          price_agent: x.price_agent,
                          photo: x.photo,
                          recommendation: x.recommendation,
                          description: x.description,
                          status: `${parseInt(x.status) + 1}`,
                          ordering: x.ordering
                      }
                    : x
            );
        } else {
            addToCarts = [
                ...addToCart,
                {
                    id: filters_.id,
                    product_name: filters_.product_name,
                    category: filters_.category,
                    type: filters_.type,
                    item: filters_.item,
                    weight: filters_.weight,
                    sku: filters_.sku,
                    price_sell: filters_.price_sell,
                    price_sell_input: filters_.price_sell_input,
                    price_promo: filters_.price_promo,
                    price_agent: filters_.price_agent,
                    photo: filters_.photo,
                    recommendation: filters_.recommendation,
                    description: filters_.description,
                    status: filters_.status,
                    ordering: filters_.ordering
                }
            ];
        }

        if (!parseInt(_total_item)) {
            total_item = addToCarts.length;
        } else {
            total_item = _total_item;
            if (!check) {
                total_item = parseInt(total_item) + 1;
            }
        }

        const get_products = addToCarts.filter(function(x) {
            return x.id === args;
        })[0];

        // Get Quantity
        let getStatusProducts: any[] = []; // status
        if (!parseInt(_quantity)) {
            quantity = total_item + parseInt(get_products.status); // First Output 2 for total_item + status
        } else {
            addToCarts.map(x => getStatusProducts.push(parseInt(x.status)));
            quantity =
                total_item +
                getStatusProducts.reduce(function(a, b) {
                    return a + b;
                });
        }

        // Get Sub Total
        let getSubtotalProducts: any[] = [];
        addToCarts.map(x => {
            getSubtotalProducts.push(x.price_sell_input * parseInt(x.status));
        });

        const total = getSubtotalProducts.reduce(function(a, b) {
            return a + b;
        });

        sub_total = `Rp ${parseInt(total)
            .toFixed(2)
            .replace(/\d(?=(\d{3})+\.)/g, "$&,")},-`;

        return dispatch({
            type: ProductTypes.ADD_CART,
            payload: {
                total_item,
                sub_total,
                quantity,
                product: addToCarts
            }
        });
    };
};

export const removeToCart = (args: string, cart: Product[]) => {
    return async (dispatch: Dispatch) => {
        if (
            !cart.filter(function(x) {
                return x.id === args;
            })[0]
        ) {
            return;
        }
        let total_item,
            sub_total,
            quantity,
            product: Product[],
            soft: Product[];
        const filters = cart.filter(x => {
            if (x.id === args) {
                return {
                    status: `${parseInt(x.status) - 1}`
                };
            }
        })[0];

        filters.status = `${parseInt(filters.status) - 1}`;

        // Quantity

        if (!parseInt(filters.status)) {
            product = cart.filter(function(x) {
                return x.id !== args;
            });
        } else {
            product = cart.map(x =>
                x.id === args
                    ? {
                          id: x.id,
                          product_name: x.product_name,
                          category: x.category,
                          type: x.type,
                          item: x.item,
                          weight: x.weight,
                          sku: x.sku,
                          price_sell: x.price_sell,
                          price_sell_input: x.price_sell_input,
                          price_promo: x.price_promo,
                          price_agent: x.price_agent,
                          photo: x.photo,
                          recommendation: x.recommendation,
                          description: x.description,
                          status: `${parseInt(x.status) - 1}`,
                          ordering: x.ordering
                      }
                    : x
            );
        }

        let getStatusProducts: any[] = [];
        let getSubTotal: any[] = [];

        if (product.length) {
            total_item = product.length;
            product.map(x => getStatusProducts.push(parseInt(x.status)));
            console.log(total_item, getStatusProducts, product.length);
            quantity =
                total_item +
                getStatusProducts.reduce(function(a, b) {
                    return a + b;
                });
            product.map(x =>
                getSubTotal.push(x.price_sell_input * parseInt(x.status))
            );
            sub_total = `Rp ${parseInt(
                getSubTotal.reduce(function(a, b) {
                    return a + b;
                })
            )
                .toFixed(2)
                .replace(/\d(?=(\d{3})+\.)/g, "$&,")},-`;
        } else {
            total_item = 0;
            quantity = 0;
            sub_total = "Rp 0,-";
        }

        return dispatch({
            type: ProductTypes.REMOVE_CART,
            payload: {
                total_item,
                sub_total,
                quantity,
                product
            }
        });
    };
};
