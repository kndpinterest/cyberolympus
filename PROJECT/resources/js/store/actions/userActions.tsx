import axios, { AxiosResponse } from "axios";
import React from "react";
import { Dispatch } from "redux";
import { LoginScreenType } from "../../routes";
import { controllerNetwork } from "../../utils/network";
import { MessageTypes } from "../constant/messageTypes";
import { User } from "../constant/types";
import { UserTypes } from "../constant/userTypes";
import { ValidateRecordUser } from "../validate/authValidate";

export const loginUser = (args: User) => {
    return async (dispatch: Dispatch) => {
        const response = await axios
            .post(`${controllerNetwork}/api/users/login`, args, {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "POST",
                    "Access-Control-Allow-Headers":
                        "Content-Type, Origin, Accepted, X-Requested-With",
                    "Access-Control-Allow-Origin": controllerNetwork
                },
                timeout: 865000,
                responseType: "json",
                withCredentials: false,
                maxContentLength: 2000,
                maxRedirects: 5,
                validateStatus: (status: number) =>
                    status >= 200 && status < 300,
                transformRequest: function request(res) {
                    dispatch({
                        type: MessageTypes.VISIBILITY
                    });
                    return JSON.stringify(res);
                }
            })
            .then((res: AxiosResponse<any>) => {
                dispatch({
                    type: UserTypes.LOGIN,
                    payload: {
                        token: res.data.access_token
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: MessageTypes.OPEN,
                    payload: {
                        message: err.response.data.message,
                        status: "open",
                        statusCode: err.response.status
                    }
                });
            });
        return response;
    };
};

export const registerUser = (
    args: User,
    setScreen: React.Dispatch<React.SetStateAction<LoginScreenType>>,
    setArgs: React.Dispatch<React.SetStateAction<User>>
) => {
    return async (dispatch: Dispatch) => {
        let active: boolean = false,
            last_name;
        if (args.name) {
            const length = args.name.split(" ").length - 1 >= 1;
            if (length) {
                const splits = args.name.split(" ");
                splits.shift();
                last_name = splits.reduce(function(a, b) {
                    return a + " " + b;
                });
            }
        }

        const response = await axios
            .post(
                `${controllerNetwork}/api/users/register`,
                {
                    first_name: args.name.split(" ")[0],
                    last_name: length ? last_name : "",
                    email: args.email,
                    password: args.password,
                    password_confirmation: args.confirm_password,
                    pin: args.pin,
                    confirm_pin: args.confirm_pin
                },
                {
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Methods": "POST",
                        "Access-Control-Allow-Origin": controllerNetwork,
                        "Access-Control-Allow-Headers":
                            "Content-Type, Origin, Accepted, X-Requested-With"
                    },
                    timeout: 865000,
                    responseType: "json",
                    withCredentials: false,
                    maxRedirects: 5,
                    maxContentLength: 2000,
                    validateStatus: (status: number) =>
                        status >= 201 && status < 300,
                    transformRequest: function request(res) {
                        dispatch({
                            type: MessageTypes.VISIBILITY
                        });
                        return JSON.stringify(res);
                    }
                }
            )
            .then((res: AxiosResponse<any>) => {
                dispatch({
                    type: UserTypes.REGISTER
                });
                setArgs({
                    ...args,
                    id: "",
                    name: "",
                    email: "",
                    password: "",
                    confirm_password: "",
                    pin: "",
                    confirm_pin: ""
                });
                dispatch({
                    type: MessageTypes.OPEN,
                    payload: {
                        message: res.data.message,
                        status: "open",
                        statusCode: res.status
                    }
                });
                setScreen("login");
            })
            .catch(err => {
                dispatch({
                    type: MessageTypes.OPEN,
                    payload: {
                        message: ValidateRecordUser.validate(err.response.data),
                        status: "open",
                        statusCode: err.response.status
                    }
                });
            });
        return response;
    };
};

export const loadMe = () => {
    return async (dispatch: Dispatch) => {
        const response = await axios
            .get(`${controllerNetwork}/api/users/me`, {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "POST",
                    "Access-Control-Allow-Origin": controllerNetwork,
                    "Access-Control-Allow-Headers":
                        "Content-Type, Origin, Accepted, X-Requested-With",
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                },
                timeout: 865000,
                responseType: "json",
                withCredentials: false,
                maxRedirects: 5,
                maxContentLength: 2000,
                validateStatus: (status: number) =>
                    status >= 200 && status < 300,
                transformRequest: function request(res) {
                    dispatch({
                        type: MessageTypes.VISIBILITY
                    });
                    return JSON.stringify(res);
                }
            })
            .then((res: AxiosResponse<any>) => {
                dispatch({
                    type: UserTypes.LOAD_ME,
                    payload: {
                        me: res.data.data
                    }
                });
            })
            .catch(err => {
                localStorage.clear();
                window.location.reload();
            });
        return response;
    };
};
