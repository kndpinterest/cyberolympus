export interface MessageUser {
    email: any[string];
    pin: any[string];
    message: string;
    password: any[string];
}

class ValidateUser {
    validate(message: MessageUser) {
        if (message.email) {
            return message.email[0];
        } else if (message.pin) {
            return message.pin[0];
        } else if (message.message) {
            return message.message;
        } else if (message.password) {
            return message.password[0];
        }
    }
}

export const ValidateRecordUser = new ValidateUser();
