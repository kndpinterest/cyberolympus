import { applyMiddleware, createStore, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import loggerMiddleware from "redux-logger";
import stores from "..";
import { createBrowserHistory } from "history";
import { routerMiddleware, RouterState } from "connected-react-router";
import { MessageState } from "../constant/messageTypes";
import { UserState } from "../constant/userTypes";
import { ProductState } from "../constant/productTypes";

export interface ApplicationState {
    router: RouterState;
    message: MessageState;
    user: UserState;
    product: ProductState;
}

export const history = createBrowserHistory();

export default function configureStore(preloadedState?: any) {
    const middleware = [thunkMiddleware];
    const store: Store<ApplicationState> = createStore(
        stores(history),
        preloadedState,
        composeWithDevTools(
            applyMiddleware(
                ...middleware,
                loggerMiddleware,
                routerMiddleware(history)
            )
        )
    );
    if (module.hot) {
        module.hot.accept(() => {
            store.replaceReducer(stores(history));
        });
    }
    return store;
}
