import React from "react";
import { useDispatch } from "react-redux";
import { MessageTypes } from "../../store/constant/messageTypes";
import { Status } from "../../store/constant/types";

interface Context {
    status: Status;
    statusCode: number;
    message: string;
}

export const MessageContext = React.createContext<Partial<Context>>({});

export const MessageContextApp = () => {
    const dispatch = useDispatch();
    const handleClickClose = (args: React.MouseEvent<HTMLButtonElement>) => {
        args.preventDefault();
        dispatch({
            type: MessageTypes.CLOSE
        });
    };
    return (
        <MessageContext.Consumer>
            {({ status, statusCode, message }) => {
                return (
                    <div
                        className={
                            statusCode <= 201
                                ? "kkn-message-success"
                                : "kkn-message-error"
                        }
                        id={
                            status === "open"
                                ? "message-open"
                                : status === "close"
                                ? "message-close"
                                : "visibility"
                        }
                        style={
                            message.length <= 50 ? { alignItems: "center" } : {}
                        }
                    >
                        <span>{message}</span>
                        <button
                            onClick={handleClickClose}
                            className="kkn-btn-message"
                        >
                            <i
                                id={
                                    statusCode <= 201
                                        ? "kkn-color-icon-success"
                                        : "kkn-color-icon-error"
                                }
                                className="fas fa-times"
                            ></i>
                        </button>
                    </div>
                );
            }}
        </MessageContext.Consumer>
    );
};
