import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { LoginScreenType } from "../../routes";
import { ApplicationState } from "../../store/configureStore";
import { User } from "../../store/constant/types";

export interface NavbarComponentProps {
    screen: LoginScreenType;
    setScreen: React.Dispatch<React.SetStateAction<LoginScreenType>>;
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
}

const NavbarComponent: React.FC<NavbarComponentProps> = (
    props: React.PropsWithChildren<NavbarComponentProps>
) => {
    const history = useHistory();
    const user = useSelector((state: ApplicationState) => state.user.data);
    console.log(user);

    const handleClickScreen = (args: LoginScreenType) => {
        history.push("/accounts");
        props.setScreen(args);
        props.setUser({
            ...props.user,
            id: "",
            name: "",
            email: "",
            password: "",
            confirm_password: "",
            pin: "",
            confirm_pin: ""
        });
    };

    let right_nav;

    if (localStorage.getItem("token")) {
        right_nav = (
            <div>
                <div className="kkn-nav-right-list">
                    <img
                        src={user.photo}
                        alt=""
                        className="kkn-nav-image-author"
                    />
                    <div className="kkn-nav-author-list">
                        <span className="kkn-nav-nickname">
                            {user.first_name} {user.last_login}
                        </span>
                        <span className="kkn-nav-email">{user.email}</span>
                    </div>
                </div>
            </div>
        );
    } else {
        right_nav = (
            <div className="kkn-nav-right-list">
                <button
                    className="kkn-btn-general"
                    onClick={handleClickScreen.bind("", "login")}
                >
                    Sign in
                </button>
                <button
                    className="kkn-btn-general"
                    onClick={handleClickScreen.bind("", "register")}
                >
                    Sign up
                </button>
            </div>
        );
    }

    return (
        <div className="kkn-nav">
            <img
                src="https://career.untar.ac.id/images/logo/76688360748Untitled.png"
                alt=""
                className="kkn-nav-logo"
            />
            {right_nav}
        </div>
    );
};

export default NavbarComponent;
