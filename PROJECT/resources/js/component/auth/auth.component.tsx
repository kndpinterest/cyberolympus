import React from "react";
import { LoginScreenType } from "../../routes";
import { User } from "../../store/constant/types";
import { AuthContext, AuthContextApp } from "./context";

export interface AuthComponentProps {
    screen: LoginScreenType;
    setScreen: React.Dispatch<React.SetStateAction<LoginScreenType>>;
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
}

const AuthComponent: React.FC<AuthComponentProps> = (
    props: React.PropsWithChildren<AuthComponentProps>
) => {
    return (
        <AuthContext.Provider
            value={{
                screen: props.screen,
                setScreen: props.setScreen,
                user: props.user,
                setUser: props.setUser
            }}
        >
            <div className="kkn-form">
                <AuthContextApp />
            </div>
        </AuthContext.Provider>
    );
};

export default AuthComponent;
