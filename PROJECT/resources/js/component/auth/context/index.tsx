import React from "react";
import { useDispatch } from "react-redux";
import { LoginScreenType } from "../../../routes";
import { loginUser, registerUser } from "../../../store/actions/userActions";
import { User } from "../../../store/constant/types";

export interface Context {
    screen: LoginScreenType;
    setScreen: React.Dispatch<React.SetStateAction<LoginScreenType>>;
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
}

export const AuthContext = React.createContext<Partial<Context>>({});
export const AuthContextApp = () => {
    const context = React.useContext(AuthContext);
    const dispatch = useDispatch();
    const handleClickScreen = (args: LoginScreenType) => {
        context.setScreen(args);
    };

    const changeId = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            id: args.currentTarget.value
        });
    };
    const changeUsername = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            name: args.currentTarget.value
        });
    };
    const changeEmail = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            email: args.currentTarget.value
        });
    };
    const changePassword = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            password: args.currentTarget.value
        });
    };
    const changeConfirmPassword = (
        args: React.ChangeEvent<HTMLInputElement>
    ) => {
        context.setUser({
            ...context.user,
            confirm_password: args.currentTarget.value
        });
    };
    const changePin = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            pin: args.currentTarget.value
        });
    };
    const changeConfirmPin = (args: React.ChangeEvent<HTMLInputElement>) => {
        context.setUser({
            ...context.user,
            confirm_pin: args.currentTarget.value
        });
    };

    const onLogin = (args: React.FormEvent<HTMLFormElement>) => {
        args.preventDefault();
        dispatch(loginUser(context.user));
    };
    const onRegister = (args: React.FormEvent<HTMLFormElement>) => {
        args.preventDefault();
        dispatch(
            registerUser(context.user, context.setScreen, context.setUser)
        );
    };
    return (
        <AuthContext.Consumer>
            {({ screen }) => {
                if (screen === "login") {
                    return (
                        <form onSubmit={onLogin}>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-user-circle"></i>
                                <input
                                    type="text"
                                    name="id"
                                    id="id"
                                    placeholder="ID"
                                    value={context.user.id || ""}
                                    onChange={changeId}
                                />
                            </div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-lock"></i>
                                <input
                                    type="password"
                                    name="pin"
                                    id="pin"
                                    placeholder="PIN"
                                    value={context.user.pin || ""}
                                    onChange={changePin}
                                />
                            </div>
                            <div className="kkn-field">
                                <button type="submit" className="kkn-btn-form">
                                    Log in
                                </button>
                            </div>
                        </form>
                    );
                } else if (screen === "register") {
                    return (
                        <div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-user-circle"></i>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="Username"
                                    value={context.user.name || ""}
                                    onChange={changeUsername}
                                />
                            </div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-at"></i>
                                <input
                                    type="text"
                                    name="email"
                                    id="email"
                                    placeholder="Email"
                                    value={context.user.email || ""}
                                    onChange={changeEmail}
                                />
                            </div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-lock"></i>
                                <input
                                    type="password"
                                    name="password"
                                    id="password"
                                    placeholder="Password"
                                    value={context.user.password || ""}
                                    onChange={changePassword}
                                />
                            </div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-lock"></i>
                                <input
                                    type="password"
                                    name="confirm_password"
                                    id="confirm_password"
                                    placeholder="Confirm Password"
                                    value={context.user.confirm_password || ""}
                                    onChange={changeConfirmPassword}
                                />
                            </div>
                            <div
                                className="kkn-field"
                                onClick={handleClickScreen.bind("", "next")}
                            >
                                <button className="kkn-btn-form">
                                    Register
                                </button>
                            </div>
                        </div>
                    );
                } else if (screen === "next") {
                    return (
                        <form onSubmit={onRegister}>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-key"></i>
                                <input
                                    type="text"
                                    name="password"
                                    id="pin"
                                    placeholder="PIN"
                                    value={context.user.pin || ""}
                                    onChange={changePin}
                                />
                            </div>
                            <div className="kkn-field" id="kkn-field">
                                <i className="fas fa-key"></i>
                                <input
                                    type="text"
                                    name="confirm_pin"
                                    id="confirm_pin"
                                    placeholder="Confirm PIN"
                                    value={context.user.confirm_pin || ""}
                                    onChange={changeConfirmPin}
                                />
                            </div>
                            <div className="kkn-field">
                                <button type="submit" className="kkn-btn-form">
                                    Done
                                </button>
                            </div>
                            <div className="kkn-field">
                                <a
                                    onClick={handleClickScreen.bind(
                                        "",
                                        "register"
                                    )}
                                >
                                    Back
                                </a>
                            </div>
                        </form>
                    );
                }
            }}
        </AuthContext.Consumer>
    );
};
