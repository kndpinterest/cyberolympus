import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    addCart,
    loadProduct,
    removeToCart
} from "../../store/actions/productActions";
import { ApplicationState } from "../../store/configureStore";
import _ from "lodash";

const ProductComponent = () => {
    const selector = useSelector((state: ApplicationState) => state.product);
    const dispatch = useDispatch();
    React.useEffect(() => {
        let mounted = true;
        if (mounted) {
            dispatch(loadProduct());
        }
        return () => {
            mounted = false;
        };
    }, []);

    const addToCarts = (args: string) => {
        dispatch(
            addCart(
                args,
                selector.data.data,
                selector.cart.total_item,
                selector.cart.sub_total,
                selector.cart.quantity,
                selector.cart.product
            )
        );
    };

    const removeToCarts = (args: string) => {
        dispatch(removeToCart(args, selector.cart.product));
    };
    return (
        <div className="kkn-grid">
            <div className="kkn-col">
                <span className="kkn-products-list-name">Product List</span>
                {_.map(selector.data.data, (base, index) => (
                    <div className="kkn-card-products" key={index}>
                        <img
                            src="https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/09/Ayam-peliharaan.jpg"
                            alt=""
                        />
                        <div className="kkn-card-list-product">
                            <div className="kkn-card-list-product-names">
                                <span className="kkn-card-name-product">
                                    {base.product_name}
                                </span>
                                <span className="kkn-card-name-category-product">
                                    {base.category}
                                </span>
                            </div>
                            <div className="kkn-card-ranting-product">
                                <i className="fas fa-star"></i>
                                <span className="kkn-card-ranting-value-product">
                                    (0)
                                </span>
                            </div>
                            <span className="kkn-card-price-product">
                                {base.price_sell}
                            </span>
                        </div>
                        <div className="kkn-card-products-cart">
                            <div className="kkn-card-products-cart-group">
                                <span
                                    className="kkn-card-products-minus"
                                    onClick={removeToCarts.bind(base, base.id)}
                                >
                                    -
                                </span>
                                <div className="kkn-card-products-total">
                                    {base.status}
                                </div>
                                <span
                                    className="kkn-card-products-plus"
                                    onClick={addToCarts.bind(base, base.id)}
                                >
                                    +
                                </span>
                            </div>
                            <button className="kkn-card-products-btn">
                                Add Cart
                            </button>
                        </div>
                    </div>
                ))}
            </div>
            <div className="kkn-col">
                {_.map(selector.cart.product, (base, index) => (
                    <div className="kkn-card-products" key={index}>
                        <img
                            src="https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/09/Ayam-peliharaan.jpg"
                            alt=""
                        />
                        <div className="kkn-card-list-product">
                            <div className="kkn-card-list-product-names">
                                <span className="kkn-card-name-product">
                                    {base.product_name}
                                </span>
                                <span className="kkn-card-name-category-product">
                                    {base.category}
                                </span>
                            </div>
                            <div className="kkn-card-ranting-product">
                                <i className="fas fa-star"></i>
                                <span className="kkn-card-ranting-value-product">
                                    (0)
                                </span>
                            </div>
                            <span className="kkn-card-price-product">
                                {base.price_sell}
                            </span>
                        </div>
                        <div className="kkn-card-products-cart">
                            <div className="kkn-card-products-cart-group">
                                <span className="kkn-card-products-minus">
                                    -
                                </span>
                                <div className="kkn-card-products-total">
                                    {base.status}
                                </div>
                                <span
                                    className="kkn-card-products-plus"
                                    onClick={addToCarts.bind(base, base.id)}
                                >
                                    +
                                </span>
                            </div>
                            <button className="kkn-card-products-btn">
                                Add Cart
                            </button>
                        </div>
                    </div>
                ))}
                <div className="kkn-products-total">
                    <div className="kkn-products-total-list">
                        <span className="kkn-products-total-list-name">
                            Total Item
                        </span>
                        <span className="kkn-products-total-list-output">
                            {selector.cart.total_item}
                        </span>
                    </div>
                    <div className="kkn-products-total-list">
                        <span className="kkn-products-total-list-name">
                            Total Quantity
                        </span>
                        <span className="kkn-products-total-list-output">
                            {selector.cart.quantity}
                        </span>
                    </div>
                    <div className="kkn-products-total-list">
                        <span className="kkn-products-total-list-name">
                            Sub Total
                        </span>
                        <span className="kkn-products-total-list-output">
                            {selector.cart.sub_total}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductComponent;
