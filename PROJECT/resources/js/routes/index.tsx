import { ConnectedRouter } from "connected-react-router";
import React from "react";
import { ApplicationState, history } from "../store/configureStore";
import { Switch, Route, Redirect } from "react-router-dom";
import HomeComponent from "../component/home/home.component";
import AuthComponent from "../component/auth/auth.component";
import NavbarComponent from "../component/navbar/navbar.component";
import { User } from "../store/constant/types";
import { MessageContext, MessageContextApp } from "../component/message";
import { useDispatch, useSelector } from "react-redux";
import { loadMe } from "../store/actions/userActions";
import ProductComponent from "../component/product/product.component";

export type LoginScreenType = "login" | "register" | "forgotted" | "next";

const Routes = () => {
    const dispatch = useDispatch();
    const selector = useSelector((state: ApplicationState) => state);
    const [screen, setScreen] = React.useState<LoginScreenType>("login");
    const [state, setState] = React.useState<User>({});

    React.useEffect(() => {
        if (selector.user.token) {
            localStorage.setItem("token", selector.user.token);
            window.location.reload();
        }
    }, [selector.user.token]);

    React.useEffect(() => {
        let mounted = true;
        if (mounted) {
            if (localStorage.getItem("token")) {
                dispatch(loadMe());
            }
        }
        return () => {
            mounted = false;
        };
    }, []);

    return (
        <MessageContext.Provider
            value={{
                message: selector.message.message.message,
                status: selector.message.message.status,
                statusCode: selector.message.message.statusCode
            }}
        >
            <ConnectedRouter history={history}>
                <NavbarComponent
                    screen={screen}
                    setScreen={setScreen}
                    user={state}
                    setUser={setState}
                />
                <Switch>
                    <Route path="/" exact={true} component={HomeComponent} />
                    <Route
                        path="/accounts"
                        render={({ location }) =>
                            localStorage.getItem("token") ? (
                                <Redirect
                                    to={{
                                        pathname: "/products",
                                        state: { from: location }
                                    }}
                                />
                            ) : (
                                <AuthComponent
                                    screen={screen}
                                    setScreen={setScreen}
                                    user={state}
                                    setUser={setState}
                                />
                            )
                        }
                    />
                    <Route
                        path="/products"
                        render={({ location }) =>
                            localStorage.getItem("token") ? (
                                <ProductComponent />
                            ) : (
                                <Redirect
                                    to={{
                                        pathname: "/accounts",
                                        state: { from: location }
                                    }}
                                />
                            )
                        }
                    />
                    <Route path="*">
                        <Redirect
                            to={{ pathname: "/", state: { from: location } }}
                        />
                    </Route>
                </Switch>
                <MessageContextApp />
            </ConnectedRouter>
        </MessageContext.Provider>
    );
};

export default Routes;
